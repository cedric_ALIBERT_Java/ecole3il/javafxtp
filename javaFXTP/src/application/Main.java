package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	
	private static Manager manager;
	
	public static Manager getManager() {
		return manager;
	}

	public void start(Stage primaryStage) {
		
		try {
			manager = new Manager(primaryStage);
			manager.initPrimaryStage();
			manager.showPaneMemoListe();
			//manager.showPaneMemoMaj();
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
