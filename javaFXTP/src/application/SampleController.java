package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;


public class SampleController implements Initializable{
	
	private Manager manager;
	@FXML
	private ListView<Memo> maListe;
	@FXML
	private MenuItem IAjouter;
	@FXML
	private MenuItem IModifier;
	@FXML
	private MenuItem ISupprimer;
	@FXML
	private MenuItem CAjouter;
	@FXML
	private MenuItem CModifier;
	@FXML
	private MenuItem CSupprimer;
	@FXML
	private Button bAjouter;
	@FXML
	private Button bModifier;
	@FXML
	private Button bSupprimer;
	@FXML
	private void doAjouter()
	{
		System.out.println("Ajouter");
		//manager.showNothing();
		model.preparerAjouter();
		manager.showPaneMemoMaj();
	}
	@FXML
	private void doModifier()
	{
		System.out.println("modifier");
		System.out.println(maListe.getSelectionModel().getSelectedItem().toString());
		model.preparerModifier(maListe.getSelectionModel().getSelectedItem());
		manager.showPaneMemoMaj();
	}
	@FXML
	private void doSupprimer()
	{
		System.out.println("supprimer");
		System.out.println(maListe.getSelectionModel().getSelectedItems().toString());
		model.supprimerMemos(maListe.getSelectionModel().getSelectedItems());
	}
	@FXML
	private void doubleClic(MouseEvent event)
	{
		if(event.getButton().equals(MouseButton.PRIMARY))
		{
			if(event.getClickCount() == 2)
			{
				doModifier();
			}
		}
	}
	private Model model;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		manager = Main.getManager();
		model = manager.getModel();
		
		maListe.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		maListe.setItems(model.getObsListMemos());
		
		maListe.setCellFactory((list)->{
			return new ListCell<Memo>(){
				@Override
				protected void updateItem(Memo item,boolean empty){
					super.updateItem(item,empty);
					if(item == null)
					{
						setText(null);
					}else{
						setText(item.getTitre());
					}
				}
			};
		});
		
		bModifier.setDisable(true);
		bSupprimer.setDisable(true);
		IModifier.setDisable(true);
		ISupprimer.setDisable(true);
		CModifier.setDisable(true);
		CSupprimer.setDisable(true);
		maListe.getSelectionModel().getSelectedItems().addListener(
				(ListChangeListener<Memo>) (c) -> {
					int nbSelections = maListe.getSelectionModel().getSelectedItems().size();
					bModifier.setDisable(true);
					bSupprimer.setDisable(true);
					IModifier.setDisable(true);
					ISupprimer.setDisable(true);
					CModifier.setDisable(true);
					CSupprimer.setDisable(true);
					if(nbSelections == 1 )
					{
						bModifier.setDisable(false);
						bSupprimer.setDisable(false);
						IModifier.setDisable(false);
						ISupprimer.setDisable(false);
						CModifier.setDisable(false);
						CSupprimer.setDisable(false);
					}
					else if(nbSelections > 1)
					{
						bModifier.setDisable(true);
						bSupprimer.setDisable(false);
						IModifier.setDisable(true);
						ISupprimer.setDisable(false);
						CModifier.setDisable(true);
						CSupprimer.setDisable(false);
					}
					
				});
		
	}
	
}
