package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ControllerMemoMaj implements Initializable{
	private Manager manager;
	private Model model;
	@FXML
	private Button bAnnuler;
	@FXML 
	private Button bOK;
	@FXML
	private TextField titre;
	@FXML 
	private TextArea contenu;
	
	public void initialize(URL location, ResourceBundle resources) {
		manager = Main.getManager();
		model = manager.getModel();
		titre.textProperty().bindBidirectional(model.getPropertyTitre());
		contenu.textProperty().bindBidirectional(model.getPropertyContenu());
	}
	@FXML
	private void doAnnuler()
	{
		manager.showPaneMemoListe();
	}
	@FXML
	private void doOK()
	{
		model.validerMiseAJour();
		manager.showPaneMemoListe();
	}
}	
