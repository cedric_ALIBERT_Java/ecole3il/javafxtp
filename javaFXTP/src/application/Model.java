package application;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
	
	private boolean ajouter;
	private Memo memoAmodifier;
	private StringProperty propertyTitre = new SimpleStringProperty();
	private StringProperty propertyContenu = new SimpleStringProperty();
	private ObservableList<Memo> obsListMemos = FXCollections.observableArrayList();
	public StringProperty getPropertyTitre() {
		return propertyTitre;
	}
	public StringProperty getPropertyContenu() {
		return propertyContenu;
	}
	public ObservableList<Memo> getObsListMemos() {
		return obsListMemos;
	}
	public void preparerAjouter()
	{
		propertyTitre.set("");
		propertyContenu.set("");
		ajouter = true;
	}
	public void validerMiseAJour()
	{
		Memo memo = new Memo();
		memo.setTitre(propertyTitre.get());
		memo.setContenu(propertyContenu.get());
		if(ajouter)
		{
			obsListMemos.add(memo);
		}
		else
		{
			obsListMemos.set(obsListMemos.indexOf(memoAmodifier),memo);
		}
		
	}
	public void preparerModifier(Memo memoAmodifier)
	{
		ajouter = false;
		propertyTitre.set(memoAmodifier.getTitre());
		propertyContenu.set(memoAmodifier.getContenu());
		this.memoAmodifier = memoAmodifier;
		
	}
	public void supprimerMemos(ObservableList<Memo> memosAsupprimer)
	{
		obsListMemos.removeAll(memosAsupprimer);
		
	}
}
