package application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Manager {
	private Stage primaryStage;
	private Pane paneMemoMaj;
	private Scene scene;
	private Pane paneMemoListe;
	private Model model = new Model();

	public Model getModel() {
		return model;
	}

	public Manager(Stage primaryStage) {
		super();
		this.primaryStage = primaryStage;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}
	public void initPrimaryStage()
	{
		
			
			try {
				//AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
				//Scene scene = new Scene(root,400,400);
				scene = new Scene(new Group());
				//Pane root = (Pane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
				//Scene scene = new Scene(root,400,400);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setMinHeight(230);
				primaryStage.setMinWidth(300);
				primaryStage.setTitle("M�mos");
				primaryStage.setScene(scene);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
	}
		//---------------------
	public void showPaneMemoListe()
	{
		if(paneMemoListe == null)
		{
			try{
				paneMemoListe = (Pane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
		}
		scene.setRoot(paneMemoListe);
	}
	public void showPaneMemoMaj()
	{
		if(paneMemoMaj == null)
		{
			try{
				paneMemoMaj = (Pane)FXMLLoader.load(getClass().getResource("PaneMemoMaj.fxml"));
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
		}
		scene.setRoot(paneMemoMaj);
	}
	public void showNothing()
	{
		scene.setRoot(new Group());
	}
}
